<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'api_token',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'api_token',
    ];

    public function applications()
    {
        return $this->belongsToMany('App\Application');
    }

    /**
     * Get the primary identifier column name.
     *
     * @return string
     */
    public function getPrimaryIdentifierName()
    {
        return 'email';
    }

    /**
     * Get the primary identifier.
     *
     * @return string
     */
    public function getPrimaryIdentifier()
    {
        return $this->getAttribute('email');
    }

    /**
     * Get the secondary identifier column name.
     *
     * @return string
     */
    public function getSecondaryIdentifierName()
    {
        return 'id';
    }

    /**
     * Get the secondary identifier.
     *
     * @return string
     */
    public function getSecondaryIdentifier()
    {
        return $this->getKey();
    }

    public function roles(){
        return $this->belongsToMany('App\Role','permissions')
            ->using('App\Participation')
            ->withPivot('expires');
    }
}
