<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = ['name','description'];

    public function application(){
        return $this->belongsTo('App\Application');
    }

    public function users(){
        return $this->belongsToMany('App\User','permissions')
            ->using('App\Participation')
            ->withPivot('expires');
    }
}
