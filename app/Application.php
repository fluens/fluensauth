<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Application extends Model
{
    use HasMediaTrait;

    protected $fillable = ['name','url'];

    public function roles(){
        return $this->hasMany('App\Role');
    }
}
