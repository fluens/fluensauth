<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @return array
     */
//    protected function rules($option)
//    {
//        if ($option=='CREATE')
//        return  [
//            'name' => 'required|string|max:255',
//            'email' => 'required|string|email|max:255|unique:users',
//            'password' => 'required|string|min:6|confirmed',
//        ];
//        elseif ($option=='UPDATE')
//            return [
//                'name' => 'required|string|max:255',
//                'email' => 'required|string|email|max:255|unique:users'
//            ];
//    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param Request $request
     * @return \App\User
     */
    protected function create(Request $request)
    {
        $validatedData=$request->validate(
            [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:6|confirmed'
            ]
//        , ['name.required'=>'Geen naam ingegeven',
//            'email.unique'=> 'Deze email is reeds in gebruik']
        );
        return User::create([
            'name' => $validatedData['name'],
            'email' => $validatedData['email'],
            'password' => bcrypt($validatedData['password']),
            'api_token' => str_random(60),
        ]);
    }


    /**
     * @param Request $request
     * @param User $user
     * @return mixed
     */
    public function update(Request $request,  User $user)
    {
        //$user=User::findOrFail($user);
        $validatedData=$request->validate(

            ['name'=>'required|max:30',
            'password'=>'nullable|max:255|confirmed',
            ]
        );

        if (array_key_exists('password', $validatedData))
        {
            return tap($user)->update([
                'name'=>$validatedData['name'],
                'password'=>bcrypt($validatedData['password'])]);
        } else
            return tap($user)->update([
                'name'=>$validatedData['name']]);

    }

    public function show(User $user)
    {
        return $user;
    }

    public function destroy(User $user)
    {
        return tap($user->delete());
    }

    public function index()
    {
        return User::all();
    }
}
