<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function update(Request $request, User $user)
    {
        $validatedData=$request->validate([
            'name'=>'required|max:30',
            'email'=>'required|email',
            'password'=>'required|max:255',
            'password_confirmation'=>'required|confirmed'
        ]);
        $user->update([
            'name'=>$validatedData['name'],
            'email'=>$validatedData['email'],
            'password'=>bcrypt($validatedData['password'])]);
    }
}
