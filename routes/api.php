<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', 'Auth\RegisterController@index');
Route::post('/register/', 'Auth\RegisterController@create');
Route::patch('/user/{user}', 'Auth\RegisterController@update');
Route::get('/user/{user}', 'Auth\RegisterController@show');

