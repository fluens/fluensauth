<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::middleware('guest')->post('/api/credentials', function (Request $request){
    if(count(User::where('email',$request->get('email'))->get()) == 0)
        return -1;
    $user = User::where('email',$request->get('email'))->first();
    if(!Hash::check($request->get('password'),$user->password)){
        return -2;
    }
    return 1;
});