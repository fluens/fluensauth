
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import Multiselect from 'vue-multiselect'
Vue.component('multiselect', Multiselect);
Vue.component('example', require('./components/Example.vue'));

Vue.component('modal', require('./modals/modal.vue'));
Vue.component('user-modal', require('./modals/user-modal.vue'));
Vue.component('app-modal', require('./modals/app-modal.vue'));

import VueGoodTablePlugin from 'vue-good-table';

// import the styles
import 'vue-good-table/dist/vue-good-table.css'
import "vue-multiselect/dist/vue-multiselect.min.css";
Vue.use(VueGoodTablePlugin);

//Passport
// Vue.component(
//     'passport-clients',
//     require('./components/passport/Clients.vue')
// );
//
// Vue.component(
//     'passport-authorized-clients',
//     require('./components/passport/AuthorizedClients.vue')
// );
//
// Vue.component(
//     'passport-personal-access-tokens',
//     require('./components/passport/PersonalAccessTokens.vue')
// );
//


const app = new Vue({
    el: '#app',
    data: function () {
        return {
            view : 'users',
            userid: null,
            usermodal : false,
            appmodal : false,
            appid: null,
            users: [
                {}
            ],
            apps: [
                {Name: 'hello', Description : 'Dit is een test', id:1}
            ],
            roles : [],
            headers: [
                {
                    label: 'Naam',
                    field: 'name',
                    filterOptions: {
                        enabled: true, // enable filter for this column
                        placeholder: 'Filteren', // placeholder for filter input
                        filterValue: '',
                    },
                },
                {
                    label: 'E-mail',
                    field: 'email',
                    filterOptions: {
                        enabled: true, // enable filter for this column
                        placeholder: 'Filteren', // placeholder for filter input
                        filterValue: '',
                    },
                },
                {
                    label:'Apps',
                    field: 'user.firm.Name',
                    filterOptions: {
                        enabled: true, // enable filter for this column
                        placeholder: 'Filteren', // placeholder for filter input
                        filterValue: '',
                    },
                },
                {
                    label: 'Rollen',
                    field: 'id',
                    filterOptions: {
                        enabled: true, // enable filter for this column
                        placeholder: 'Filteren', // placeholder for filter input
                        filterValue: '',
                    },
                },
                {
                    label: 'Actie',
                    field: 'action',
                }
            ]

        }
    },
    methods: {
        datum(rowObj) {
            return this.momentify(rowObj.date)
        },
        getApps: function() {
                axios.get('api/apps').then(response => {this.apps = response.data;});
        },
        getUsers: function() {
            axios.get('api/users').then(
                response => {
                    this.users = response.data;
                }

            );
        },
        getRoles : function() {
            axios.get('api/roles').then(
                response => {
                    this.roles = response.data;
                }

            );
        }
    },
    created() {
        this.getApps();
        this.getUsers();
        this.getRoles();
    },
    mounted() {
    }

});
