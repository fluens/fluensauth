@extends('layouts.app')

@section('content')
    <user-modal :show="usermodal" @close="usermodal = false" :id="userid"></user-modal>
    <app-modal :show="appmodal" :id="appid" @close="appmodal = false" @refresh="getApps();"></app-modal>
<div class="container" style="margin-top:10vh">
    <div class="row">
        <div class="col-md-12">
            <transition name="fade">
            <div v-if="view === 'users'" class="panel panel-default">
                <div class="panel-heading">Gebruikers
                    <button  class="btn btn-primary pull-right" @click="usermodal = true;"><i class="fa fa-plus mr-2"></i>Gebruiker</button>
                </div>

                <div class="panel-body">
                    <vue-good-table
                            :pagination-options="{
    enabled: true,nextLabel:'Volgende',prevLabel:'Vorige',rowsPerPageLabel:'Rijen per pagina', ofLabel:'van',pageLabel:'pagina',allLabel:'Alles'
  }"

                            :columns="headers"
                            :rows="users"
                            styleClass="table">
                        <template slot="table-row" slot-scope="props">
                    <span v-if="props.column.field == 'action'">
                        <span class="clickable" @click="usermodal = true;userid=props.row.id">Aanpassen</span>
                    </span>
                            <span v-else>
      @{{props.formattedRow[props.column.field]}}
    </span>
                        </template>
                    </vue-good-table>
                </div>
            </div>
            <div v-if="view === 'apps'" class="panel panel-default">
                <div class="panel-heading">Apps
                    <button  class="btn btn-primary pull-right" @click="appmodal = true;"><i class="fa fa-plus mr-2"></i>App</button>
                </div>
                <div class="panel-body" style="padding:3em">
                    <div class="row">
                        <div class="col-md-12">
                            <transition-group name="slide" tag="div" class="row mb-3">
                                <div class="col-md-4 col-lg-3 col-sm-6 mb-4" v-if="apps" v-for="(app, index) in apps" :key="index" style="    border: 1px solid #ddd;
    border-radius: 3px;
    padding: 1em;" >
                                    <div class="card">
                                        <div class="card-header">
                                            <div class="card-title d-flex justify-content-between align-items-center">
                                                <div class=" ">
                                                    <span class="small">App @{{ app.id }}</span><br>
                                                    <span class="cursor-pointer">@{{app.Name}}</span><br>
                                                    <span class="small">@{{app.Description}}</span><br>
                                                </div>
                                                <span class="clickable" @click="appmodal = true;appid=app.id"><i class="fa fa-pensil"></i>Aanpassen</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </transition-group>
                        </div>
                    </div>
                </div>
            </div>
            </transition>
        </div>
    </div>
</div>

@endsection
