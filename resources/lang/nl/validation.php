<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'boolean'              => 'Het :attribute veld moet waar of vals zijn.',
    'date'                 => 'De :attribute is geen geldige datum.',
    'date_format'          => 'De :attribute volgt niet het formaat :format.',
    'digits'               => 'Het :attribute moet :digits getallen zijn.',
    'digits_between'       => 'Het :attribute moet tussen :min en :max getallen zijn.',
    'distinct'             => 'Het :attribute bestaat al.',
    'email'                => 'Het :attribute moet een geldig emailadres zijn.',
    'file'                 => 'Het :attribute moet een bestand zijn.',
    'image'                => 'Het :attribute moet een foto zijn.',
    'max'                  => [
        'numeric' => 'Het :attribute mag niet groter dan :max zijn.',
        'file'    => 'Het :attribute mag niet groter dan :max kilobytes zijn.',
        'string'  => 'Het :attribute mag niet groter dan :max karakters zijn.',
        'array'   => 'Het :attribute mag niet meer dan :max elementen bevatten.',
    ],
    'mimes'                => 'Het :attribute bestand is niet in het juiste formaat (:mimes)',
    'min'                  => [
        'numeric' => 'Het :attribute moet minimaal :min zijn.',
        'file'    => 'Het :attribute moet minimaal :min kilobytes zijn.',
        'string'  => 'Het :attribute moet minimaal :min karakters zijn.',
        'array'   => 'Het :attribute moet minimaal :min elementen bevatten.',
    ],
    'numeric'              => 'Het :attribute moet een getal zijn.',
    'present'              => 'Het :attribute veld moet aanwezig zijn.',
    'regex'                => 'Het :attribute formaat is ongeldig.',
    'required'             => 'Het :attribute veld moet ingevuld zijn.',
    'required_if'          => 'Het :attribute veld moet ingevuld zijn als :other :value is.',
    'required_unless'      => 'Het :attribute veld moet ingevuld zijn tenzij :other :values is.',
    'same'                 => 'Het :attribute en :other moeten overeenstemmen.',
    'size'                 => [
        'numeric' => 'Het :attribute moet :size zijn.',
        'file'    => 'Het :attribute moet :size kilobytes zijn.',
        'string'  => 'Het :attribute moet :size karakters zijn.',
        'array'   => 'Het :attribute moet :size elementen bevatten.',
    ],
    'string'               => 'Het :attribute moet tekst bevatten.',
    'unique'               => 'Het :attribute is reeds in gebruik.',
    'uploaded'             => 'Het :attribute kon niet uploaden.',
    'url'                  => 'Het :attribute formaat is ongeldig.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'name'=>'naam',
        'password'=>'wachtwoord'
    ],

];
