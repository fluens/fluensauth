<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Wachtwoorden moeten minstens 6 tekens lang zijn en beide wachtwoorden moeten overeenstemmen.',
    'reset' => 'Uw wachtwoord is gereset!',
    'sent' => 'We hebben u een password reset link gemaild!',
    'token' => 'Deze password reset token is niet geldig.',
    'user' => "Geen gebruiker gevonden met het opgegeven emailadres.",

];
