<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(['name' => 'Ward', 'email' => 'ward@fluens.be', 'password' => bcrypt('secret'), 'api_token'=>str_random(60)]);
        User::create(['name' => 'Hendrik', 'email' => 'hendrik@fluens.be', 'password' => bcrypt('secret'), 'api_token'=>str_random(60)]);
        if (count(User::all())<22) {
            factory(User::class, 20)->create();
        }
    }
}
